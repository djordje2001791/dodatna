#include <iostream>
#include <math.h>
#include <cstdlib>

using namespace std;

int main()
{
    long n;
    cin >> n;

    long rez = (n%2==0) ? (n/2)-1 : floor(n/2);

    cout << rez << endl;

    system("pause");
    return 0;
}
