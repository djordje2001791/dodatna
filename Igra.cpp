#include <iostream>
#include <vector>
#include <cstdlib>
#include <algorithm>

using namespace std;

int igraFaktorizacije(int M, int* P, int N){
    vector<int> Q;
    int i,j,k;
    int temp=0;
    int a=0;

    for(i=0;i<M;i++){
        for(j=0;j<M;j++){
            a=0;
            temp = P[i]*P[j];
            for(k=0;k<Q.size();k++){
                if(temp==Q[k])
                    a++;
            }

            if(a==0){
                Q.push_back(temp);
            }
            temp = 0;
        }
    }

    sort (Q.begin(), Q.begin() + Q.size());

    return Q[N-1];
}

int main()
{
    int M = 3;
    int P[] = {7,2,5};
    int N = 4;

    cout << igraFaktorizacije(M, P, N) << endl;

    system("pause");
    return 0;
}
