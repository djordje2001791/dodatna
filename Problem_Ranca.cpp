#include <iostream>
#include <vector>

using namespace std;

typedef unsigned long long ull;

inline ull max(ull a, ull b) { return (a > b) ? a : b; }

ull knapSack(ull capacity, vector<ull> vol, vector<ull> val, ull n) {
	if (n == 0 || capacity == 0)
		return 0;

	if (vol[n - 1] > capacity)
		return knapSack(capacity, vol, val, n - 1);

	else return max(val[n - 1] + knapSack(capacity - vol[n - 1], vol, val, n - 1),
		knapSack(capacity, vol, val, n - 1)
	);
}

int main() {
	ull n, capacity;
	cin >> capacity >> n;

	vector<ull> val(n), vol(n);
	for (ull i = 0; i < n; i++)
		cin >> val[i] >> vol[i];

	cout << knapSack(capacity, vol, val, n) << endl;

	return 0;
}