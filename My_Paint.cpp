#include <iostream>
#include <cstdlib>

using namespace std;

int main()
{
    int n, m;
    cin >> n >> m;

    bool a[n][n];
    int i,j,k;

    for(i=0;i<n;i++){
        for(j=0;j<n;j++){
            a[i][j]='.';
        }
    }

    int x1, x2, y1, y2;
    for(i=0;i<m;i++){
        cin >> x1 >> y1 >> x2 >> y2;

        for(j = x1; j <= x2; j++)
            for(k = y1; k <= y2; k++)
                a[j-1][k-1] = !a[j-1][k-1];
    }

    for(i=0;i<n;i++){
        for(j=0;j<n;j++){
            if (a[i][j])
                cout << ".";
            else
                cout << "*";
        }
        cout << endl;
    }
    cout << endl;

    system("pause");
    return 0;
}
