#include <iostream>
#include <cstdlib>
#include <math.h>

using namespace std;

long sumaKvadrata(long num) {
    long total = 0;
    while(num) {
        long digit = num % 10;
        num /= 10;
        total += pow(digit,2);
    }
    return total;
}

int main()
{
    long a,b;
    cin >> a >> b;

    long temp;

    int countNum=0;
    for(long i=a;i<=b;i++){
        temp=i;
        for(long j=0;j<9;j++){
            if(sumaKvadrata(temp)==1){
                countNum++;
                break;
            }else
                temp=sumaKvadrata(temp);
        }
    }

    cout << countNum << endl;

    system("pause");
    return 0;
}
