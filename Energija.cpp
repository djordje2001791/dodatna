#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <algorithm>

using namespace std;

struct el {
	int p, c;
};

bool sortf(el a, el b) { return a.c<b.c; }

int main()
{
    int n,w;
    cin >> n;
    cin >> w;

	el e[1005];
	for (int i=0;i<n;i++)
		cin>>e[i].p>>e[i].c;
	sort (e,e+n,sortf);

	int c=0;
	for(int i=0;i<n;i++)
	{
		int n=min(w,e[i].p);
		w-=n;
		c+=n*e[i].c;
	}

	cout<<c<<endl;

    system("pause");
    return 0;
}
