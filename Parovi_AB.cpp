#include <iostream>
#include <vector>
#include <cmath>
#include <string>
 
using namespace std;
 
typedef unsigned long long ull;
 
int main() {
    ull n, k;
    cin >> n >> k;
 
    vector<char> solution(n, 'b');
 
    if ((n % 2 == 0 && k > pow(n / 2, 2)) || (n % 2 == 1 && k > (n / 2)*(n / 2 + 1)))
        cout << "x" << endl;
    else {
        ull poz = 0;
        while(k > n-poz-1){
            k -= (n-2*poz-2);
            solution[poz] = 'a';
            poz++;
        }
        solution[n-1-k] = 'a';
        
        
        for (ull i = 0; i < n; i++)
            cout << solution[i];
        cout << endl;
    }
 
}