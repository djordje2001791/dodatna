#include <bits/stdc++.h>

using namespace std;

typedef long long ull;

ull max(ull a, ull b){
    return (a>b) ? a : b;
}

int main(){
    ull x1, y1, x2, y2;
    cin >> x1 >> y1
        >> x2 >> y2;

    x1 = min(x1, x2), x2 = max(x1, x2);
    y1 = min(y1, y2), y2 = max(y1, y2);

    ull sideX = max(x2, 0) - max(x1, 0), sideY = max(y2, 0) - max(y1, 0);

    cout << sideX * sideY << endl;

    return 0;
}
