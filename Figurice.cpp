#include <iostream>
#include <vector>

using namespace std;

typedef unsigned long long ull;
typedef vector<vector<char> > matrix;

void printMatrix(matrix a) {
	for (ull i = 0; i < a.size(); i++) {
		for (ull j = 0; j < a.size(); j++)
			cout << a[i][j];
		cout << endl;
	}
}

int main() {
	ull n;
	cin >> n;
	n = pow(2, n);

	ull x, y;
	cin >> x >> y;
	x--; y--;

	matrix a(n);
	for (ull i = 0; i < n; i++)
		a[i].resize(n, '-');

	a[x][y] = '0';

	for (ull i = 0; i < n; i++)
		if (i == y)
			continue;
		else
			a[x][i] = '1' + i / 2;
	for (ull i = 0; i < n; i++)
		if (i == x)
			continue;
		else
			a[i][y] = '1' + i / 2;

	printMatrix(a);

	return 0;
}