#include <iostream>
#include <cstdlib>
#include <iomanip>

using namespace std;

void printUpOrDown(int max1, int max2, int max3){
    int i=0;

    for(i = 0; i < (3+max1) + (3+max2) + (3+max3) + 1; i++){
        if(i==0 || i==(max1+3) || i==(max1+3) + (max2+3) || i==(3+max1) + (3+max2) + (3+max3))
            cout << "+";
        else
            cout << "-";
    }
    cout << endl;
}

int numberOfDigits(int no){
    int a=0;

    while(no>0)
    {
        no=no/10;
        a++;
    }

    return a;
}
int main()
{
    int a, b;
    cin >> a >> b;

    int len = b-a+1;
    int c[len][3];

    int i, j, k;
    for(i=0, j=a;i<len;i++, j++){
        c[i][0]=j;
        c[i][1]=j*j;
        c[i][2]=j*j*j;
    }

    int max1=0, max2=0, max3=0;
    for(i=0;i<len;i++){
        max1=max(numberOfDigits(c[i][0]), max1);
        max2=max(numberOfDigits(c[i][1]), max2);
        max3=max(numberOfDigits(c[i][2]), max3);
    }

    for(i=0;i<len;i++){
        printUpOrDown(max1, max2, max3);

        for(j = 0; j < (3+max1) + (3+max2) + (3+max3); j++){
            if(j==0 || j==(max1+3) || j==(max1+3) + (max2+2) || j==(3+max1) + (2+max2) + (2+max3))
                cout << "|";
            else if(j==0 + 2)
                cout << setw(max1) << c[i][0];
            else if(j==(max1+3) + 2)
                cout << setw(max2) << c[i][1];
            else if(j==(max1+3) + (max2+3) + 1)
                cout << setw(max3) << c[i][2];
            else
                cout << " ";
        }

        cout << endl;
    }
    printUpOrDown(max1, max2, max3);
    cout << endl;

    system("pause");
    return 0;
}
