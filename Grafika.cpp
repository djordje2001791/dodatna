#include <iostream>
#include <cstdlib>

using namespace std;

int main()
{
    int a[80];
    int temp;
    int i=0;
    do{
        cin >> temp;
        a[i]=temp;
        i++;
    }while(temp!=0);

    int n=i;

    int maxNum=0;
    for(i=0;i<n;i++){
        maxNum = max(maxNum, a[i]);
    }

    for (int red=maxNum;red>=1;red--)
    {
        bool kraj = false;
        for (int i=0;i<n && !kraj;i++)
        {
            kraj = true;
            for (int j=i;j<n;j++)
            {
                if (red <= a[j])
                   kraj = false;
            }
            if (!kraj)
            {
                if (red <= a[i])
                   cout<<"x";
                else
                   cout<<" ";
            }
        }
        cout<<endl;
    }
    cout << endl;

    system("pause");
    return 0;
}
