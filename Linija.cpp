#include <cstdlib>
#include <iostream>

using namespace std;

int main()
{
    int n,m;
    cin >> n >> m;
    char a[n][m];
    int i,j,k;
    for(i=0;i<n;i++){
        for(j=0;j<m;j++){
            cin >> a[i][j];
        }
    }

    int maxNum = 0;
    int countNum = 0;
    for(i=0;i<n;i++){
        for(j=0;j<m;j++){
            countNum=0;
            if(a[i][j]=='0')
                for(k=j;k<m;k++)
                    if(a[i][k]=='0')
                            countNum++;
            maxNum=max(maxNum,countNum);
        }
    }

    cout << maxNum << endl;

    system("pause");
    return 0;
}
