#include <iostream>
#include <cstdlib>

using namespace std;

int main()
{
    int n;
    cin >> n;
    int c[n];
    int i,j;
    for(i=0;i<n;i++){
        cin >> c[i];
    }

    int q;
    cin >> q;
    int a[q];
    int b[q];
    for(i=0;i<q;i++){
        cin >> a[i];
        cin >> b[i];
    }

    int zbir[q];
    for(i=0;i<q;i++){
        zbir[i]=0;
    }

    for(i=0;i<q;i++){
        for(j=a[i]-1;j<b[i];j++){
            zbir[i]+=c[j];
        }
    }

    for(i=0;i<q;i++){
        cout << zbir[i] << endl;
    }

    system("pause");
    return 0;
}
