#include <iostream>
#include <vector>
#include <algorithm>

typedef unsigned long long ull;

ull counter = 0;

std::vector<ull> mergeArrays(std::vector<ull> &first, std::vector<ull> &second) {
    std::vector<ull> result;

    ull i1 = 0, i2 = 0;

    while(i1 < first.size() && i2 < second.size())
        if(first[i1] < second[i2])
            result.push_back(first[i1++]);
        else {
            result.push_back(second[i2++]);

            counter += first.size() - i1;
        }

    if(i1 < first.size())
        for(; i1 < first.size(); i1++)
            result.push_back(first[i1]);
    else
        for(; i2 < second.size(); i2++)
            result.push_back(second[i2]);

    return result;
}

void mergeSort(std::vector<ull> &elements) {
    if(elements.size() < 3) {
        std::sort(elements.begin(), elements.end());
        return;
    }

    std::vector<ull> firstHalf, secondHalf;

    for(ull i = 0; i < elements.size(); i++)
        if(i <= elements.size() / 2)
            firstHalf.push_back(elements[i]);
        else
            secondHalf.push_back(elements[i]);

    mergeSort(firstHalf);
    mergeSort(secondHalf);

    elements = mergeArrays(firstHalf, secondHalf);
}

int main() {
    int n;
    std::cin >> n;

    std::vector<ull> elements(n);
    for(ull i = 0; i < n; i++)
        std::cin >> elements[i];

    mergeSort(elements);

    std::cout << counter << std::endl;

    return 0;
}
