#include <iostream>
#include <algorithm>
#include <string>
#include <sstream>
#include <vector>

inline unsigned long long sumOfNums(unsigned long long x) { return x * (x + 1) / 2; }

int main() {
    unsigned long long n, numOfInversions;

    std::cin >> n >> numOfInversions;

    if (numOfInversions > sumOfNums(n - 1)) {
        std::cout << "Nemoguc slucaj!" << std::endl;
        return 1;
    }

    std::vector<int> solution;
    for (unsigned long long i = 1; i <= n; i++)
        solution.push_back(i);

    unsigned long long tempN = n - 1;

    while (numOfInversions) {
        unsigned long long maxDifference = std::min(numOfInversions, tempN);

        solution.insert(solution.begin() + n - maxDifference - 1, solution[solution.size() - 1]);
        solution.erase(solution.begin() + solution.size() - 1);

        numOfInversions -= maxDifference;

        tempN--;
    }

    for(unsigned long long i = 0; i < n; i++) {
        std::cout << solution[i];

        if(i != n - 1)
            std::cout << ", ";
    }

    std::cout << std::endl;

    return 0;
}