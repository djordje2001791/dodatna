#include <iostream>

using namespace std;

typedef unsigned short us;

int main() {
	us a, b, c;
	cin >> a >> b >> c;

	char aX, bX, cX;
	if (a == 0 && b == 0 && c == 0)
		aX = 'B', bX = 'B', cX = 'B';
	else {
		if (a == 2) {
			aX = 'B';
			bX = 'C';
			cX = 'C';
		}else if (b == 2) {
			aX = 'C';
			bX = 'B';
			cX = 'C';
		}else if (c == 2) {
			aX = 'C';
			bX = 'C';
			cX = 'B';
		}
	}

	cout << aX << endl
		<< bX << endl
		<< cX << endl;
	
	return 0;
}