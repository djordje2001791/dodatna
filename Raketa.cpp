#include <iostream>

using namespace std;

typedef unsigned long long ull;

ull abs(ull x) { return (x>0) ? x : -x; }

ull klackavost(ull x11, ull x12, ull x21, ull x22) {
    ull maxNum = max(max(x11,x12), max(x21,x22));
    ull minNum = min(min(x11,x12), min(x21,x22));

    return maxNum-minNum;
}

int main() {
    ull x11, x12, x21, x22;
    cin >> x11 >> x12 >> x21 >> x22;

    ull klackPre=klackavost(x11, x12, x21, x22), klackPos=klackPre;
    bool preslo=false;
    while(1) {
        preslo=false;

        x11++; x12++;
        klackPos=klackavost(x11, x12, x21, x22);
        if(klackPos>klackPre) {
            x11--; x12--;
        }else {
            klackPre=klackPos;
            preslo=true;
        }

        x11++; x21++;
        klackPos=klackavost(x11, x12, x21, x22);
        if(klackPos>klackPre) {
            x11--; x21--;
        }else {
            klackPre=klackPos;
            preslo=true;
        }

        x21++; x22++;
        klackPos=klackavost(x11, x12, x21, x22);
        if(klackPos>klackPre) {
            x21--; x22--;
        }else {
            klackPre=klackPos;
            preslo=true;
        }

        x12++; x22++;
        klackPos=klackavost(x11, x12, x21, x22);
        if(klackPos>klackPre) {
            x11--; x12--;
        }else {
            klackPre=klackPos;
            preslo=true;
        }

        if(preslo==false)
            break;
    }

    cout << klackPre << endl;

    return 0;
}
