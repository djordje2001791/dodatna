#include <iostream>
#include <cstdlib>
#include <vector>

using namespace std;

int main()
{
    int N, M;
    cin >> N >> M;

    int T[N*M];
    int i;
    for(i=0;i<N*M;i++){
        cin >> T[i];
    }

    int maxNum = 0;
    vector<int> red, kolona;
    red.resize(N);
    kolona.resize(M);
    for(int i = 0; i < N; i++)
        for(int j = 0; j < M; j++)
        {
            red[i] += T[M * i + j];
            kolona[j] += T[M * i + j];
        }
    for(int i = 0; i < N; i++)
        for(int j = 0; j < M; j++)
            maxNum = max(red[i] + kolona[j] - T[M * i + j], maxNum);


    cout << maxNum<< endl;

    system("pause");
    return 0;
}
