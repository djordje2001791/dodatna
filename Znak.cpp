#include <iostream>
#include <cstdlib>

using namespace std;

int main()
{
    int n;
    cin >> n;

    int a[n];
    int i;
    for(i=0;i<n;i++){
        cin >> a[i];
    }

    if(n==3){
        if(a[0]+a[1]==a[2]){
            cout << a[0] << "+" << a[1] << "=" << a[2] << endl;
            return 0;
        }else if(a[0]-a[1]==a[2]){
            cout << a[0] << "-" << a[1] << "=" << a[2] << endl;
            return 0;
        }else if(a[0]==a[1]+a[2]){
            cout << a[0] << "=" << a[1] << "+" << a[2] << endl;
            return 0;
        }else if(a[0]==a[1]-a[2]){
            cout << a[0] << "=" << a[1] << "-" << a[2] << endl;
            return 0;
        }
    }else if(n==4){
        if(a[0]+a[1]==a[2]+a[3]){
            cout << a[0] << "+" << a[1] << "=" << a[2] << "+" << a[3] << endl;
            return 0;
        }else if(a[0]+a[1]==a[2]-a[3]){
            cout << a[0] << "+" << a[1] << "=" << a[2] << "-" << a[3] << endl;
            return 0;
        }else if(a[0]-a[1]==a[2]+a[3]){
            cout << a[0] << "-" << a[1] << "=" << a[2] << "+" << a[3] << endl;
            return 0;
        }else if(a[0]-a[1]==a[2]-a[3]){
            cout << a[0] << "-" << a[1] << "=" << a[2] << "-" << a[3] << endl;
            return 0;
        /*****************************************************************/
        }else if(a[0]==a[1]+a[2]+a[3]){
            cout << a[0] << "=" << a[1] << "+" << a[2] << "+" << a[3] << endl;
            return 0;
        }else if(a[0]==a[1]+a[2]-a[3]){
            cout << a[0] << "=" << a[1] << "+" << a[2] << "-" << a[3] << endl;
            return 0;
        }else if(a[0]==a[1]-a[2]+a[3]){
            cout << a[0] << "=" << a[1] << "-" << a[2] << "+" << a[3] << endl;
            return 0;
        }else if(a[0]==a[1]-a[2]-a[3]){
            cout << a[0] << "=" << a[1] << "-" << a[2] << "-" << a[3] << endl;
            return 0;
        /*****************************************************************/
        }else if(a[0]+a[1]+a[2]==a[3]){
            cout << a[0] << "+" << a[1] << "+" << a[2] << "=" << a[3] << endl;
            return 0;
        }else if(a[0]+a[1]-a[2]==a[3]){
            cout << a[0] << "+" << a[1] << "-" << a[2] << "=" << a[3] << endl;
            return 0;
        }else if(a[0]-a[1]+a[2]==a[3]){
            cout << a[0] << "-" << a[1] << "+" << a[2] << "=" << a[3] << endl;
            return 0;
        }else if(a[0]-a[1]-a[2]==a[3]){
            cout << a[0] << "-" << a[1] << "-" << a[2] << "=" << a[3] << endl;
            return 0;
        }

    }

    system("pause");
    return 0;
}
