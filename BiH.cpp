#include <iostream>
#include <cstdlib>

using namespace std;

int main()
{
    int n, m;
    cin >> n >> m;

    short a[n][m];
    int i, j;
    for(i=0;i<n;i++){
        for(j=0;j<m;j++){
            cin >> a[i][j];
        }
    }

    short suma = 0;
    for(i=0;i<n;i++){
        for(j=0;j<m;j++){
            if(a[i][j]==1 && a[i][j+1]==1 && a[i][j+2]==0 &&
               a[i+1][j]==1 && a[i+1][j+1]==0 && a[i+1][j+2]==1 &&
               a[i+2][j]==1 && a[i+2][j+1]==1 && a[i+2][j+2]==0 &&
               a[i+3][j]==1 && a[i+3][j+1]==0 && a[i+3][j+2]==1 &&
               a[i+4][j]==1 && a[i+4][j+1]==1 && a[i+4][j+2]==0)
               suma++;

            if(a[i][j]==1 && a[i][j+1]==0 && a[i][j+2]==1 &&
               a[i+1][j]==1 && a[i+1][j+1]==0 && a[i+1][j+2]==1 &&
               a[i+2][j]==1 && a[i+2][j+1]==1 && a[i+2][j+2]==1 &&
               a[i+3][j]==1 && a[i+3][j+1]==0 && a[i+3][j+2]==1 &&
               a[i+4][j]==1 && a[i+4][j+1]==0 && a[i+4][j+2]==1)
               suma++;
        }
    }

    cout << suma << endl;


    system("pause");
    return 0;
}
