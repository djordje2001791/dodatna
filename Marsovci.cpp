#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

typedef unsigned long long ull;

bool inVector(ull minNum, ull maxNum, vector<ull> a) {
	bool minX = false, maxX = false;
	for (ull i = 0; i < a.size(); i++) {
		if (a[i] == minNum)
			minX = true;
		if (a[i] == maxNum)
			maxX = true;
	}

	return (minX && maxX) ? true : false;
}

int main() {
	ull n;
	cin >> n;

	vector<ull> a;
	a.resize(n);
	for (ull i = 0; i < n; i++) {
		cin >> a[i];
	}

	ull minNum = a[0], maxNum = a[0];
	for (ull i = 0; i < n; i++) {
		minNum = min(minNum, a[i]);
		maxNum = max(maxNum, a[i]);
	}

	ull count = 0;
	for (ull i = 0; i < n - 1; i++)
		for (ull j = i + 1; j < n; j++) {
			vector<ull> k;
			for (ull m = i; m <= j; m++)
				k.push_back(a[m]);

			if (inVector(minNum, maxNum, k))
				count++;
		}

	cout << count << endl;

	return 0;
}