#include <iostream>
#include <vector>
#include <utility>
#include <string>

typedef unsigned long long ull;
typedef std::pair<std::string, std::string> pss;
typedef std::vector<ull> v_ull;
typedef std::vector<pss> v_pss;

int main() {
    int n, d;
    std::cin >> n >> d;

    v_pss times(3);
    for(ull i = 0; i < n; i++)
        std::cin >> times[i].first >> times[i].second;

    return 0;
}