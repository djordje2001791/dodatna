//U matrici se nalaze neke prepreke i potrebno je naci duzinu stranice najdruzeg kvadrata bez prepreka

#include <iostream>
#include <vector>
#include <minmax.h>

using namespace std;

typedef unsigned long long ull;

int main() {
	ull n, p;
	cin >> n >> p;

	vector<vector<unsigned short> > field;
	field.resize(n);

	for (ull i = 0; i < n; i++)
		field[i].resize(n);

	for (ull i = 0; i < n; i++)
		for (ull j = 0; j < n; j++)
			field[i][j]=1;

	

	for (ull i = 0; i < p; i++) {
		ull tempX = 0, tempY = 0;
		cin >> tempX >> tempY;
		field[tempX][tempY] = 0;
	}

	for (ull i = 1; i < n; i++)
		for (ull j = 1; j < n; j++)
			if (field[i - 1][j - 1] != 0 && field[i][j] != 0)
				field[i][j] = min(field[i - 1][j], field[i][j - 1]) + 1;

	ull maxLength = 0;
	for (ull i = 0; i < n; i++)
		for (ull j = 0; j < n; j++)
			maxLength = max(maxLength, field[i][j]);

	cout << maxLength << endl;

	return 0;
}