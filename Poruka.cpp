#include <iostream>
#include <cstdlib>
#include <sstream>
#include <string>

using namespace std;

int stringToInt(string a){
	int b;
	istringstream ss(a);
	ss >> b;

	return b;
}

int binaryToInt(int num){
    long dec = 0, rem, base = 1;
    while (num > 0)
    {
        rem = num % 10;
        dec = dec + rem * base;
        base = base * 2;
        num = num / 10;
    }

    return dec;
}

int main()
{
    int n;
    cin >> n;

    string a[n];
    int i,j;
    for(i=0;i<n;i++){
        cin >> a[i];
    }

    string izdvojeniString;
    for(i=0;i<n;i++){
        for(j=0;j<a[i].length();j++){
            if(a[i].substr(j,1)=="o"){
                izdvojeniString+="1";
            }
            else if(a[i].substr(j,1)=="."){
                izdvojeniString+="0";
            }
        }
    }


    int len = izdvojeniString.length();

    string asciiSimboli[50];
    j=0;
    for(i=0;i<len;i+=7){
        asciiSimboli[j]=izdvojeniString.substr(i,7);
        j++;
    }

    char asciiSimboliZnak[50];
    for(i=0;i<j+1;i++){
        int temp = stringToInt(asciiSimboli[i]);
        int temp1 = binaryToInt(temp);
        asciiSimboliZnak[i]=temp1;

        cout << asciiSimboliZnak[i];
    }

    cout << "\b " << endl;

    system("pause");
    return 0;
}
