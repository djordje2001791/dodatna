#include <iostream>

using namespace std;

typedef unsigned long long ull;

int main() {
	ull n, m;
	cin >> n >> m;

	ull count = 0;
	if (n == m)
		count = 1;
	else {
		while (1) {
			//Pravi se kvadrat stranice min(n, m) kockica
			count++;
			if (n > m)
				n -= m;
			else if (n < m)
				m -= n;
			else
				break;
		}
	}

	cout << count << endl;
}