#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <string>
#include <vector>
#include <sstream>
#include <math.h>

using namespace std;

long long stringToInt(string a){
	long long b;

	istringstream ss(a);
	ss >> b;

	return b;
}

string intToString(long long a){
	string b;

	stringstream ss;
	ss << a;
	b = ss.str();

    return b;
}

int main()
{
    string temp1;
    getline(cin, temp1);

    string s;
    getline(cin, s);

    string buf;
    stringstream ss(s);

    vector<string> a;

    while (ss >> buf)
        a.push_back(buf);

    long long i;
    for(i=0;i<a.size();i++){
        if(a[i].substr(0,1)=="-")
            continue;
        else if(a[i].substr(0,1)=="0" && a[i]!=a[i].substr(0,1))
            a[i]=a[i].substr(1,a[i].length()-1);
        if(intToString(round(stringToInt(a[i])))==a[i])
            cout << a[i] << endl;
    }


    system("pause");
    return 0;
}
