#include <iostream>
#include <sstream>

using namespace std;

typedef unsigned long long ull;

string toString(ull x) {
	stringstream ss;
	ss << x;
	return ss.str();
}

ull power(ull a, ull b) {
	ull result = 1;
	for (ull i = 0; i < b; i++) {
		result *= a;
	}

	return result;
}

int main() {
	ull n, m, k;
	cin >> n >> m >> k;

	ull result = 1;
	for (ull i = 0; i < m; i++) {
		result *= n;
		result = result % power(10, k);
	}

	for (ull i = 0; i < (k-toString(result).length()); i++)
		cout << "0";
	cout << result << endl;

	return 0;
}