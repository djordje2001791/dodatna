#include <iostream>
#include <cstdlib>
#include <vector>
#include <math.h>
#include <algorithm>

using namespace std;

int main()
{
    const int n = 35;

    vector<long long> stepeniBrojaTri;
    int i;
    for(i=0;i<n;i++)
        stepeniBrojaTri.push_back(pow(3,i));

    vector<long long> dvaSabirkaBrojaTri;
    int j;
    for(i=0;i<n;i++)
        for(j=i+1;j<n+1;j++)
            dvaSabirkaBrojaTri.push_back(pow(3,i)+pow(3,j));

    vector<long long> triSabirkaBrojaTri;
    int k;
    for(i=0;i<n;i++)
        for(j=i+1;j<n+1;j++)
            for(k=j+1;k<n+2;k++)
                triSabirkaBrojaTri.push_back(pow(3,i)+pow(3,j)+pow(3,k));

    vector<long long> cetiriSabirkaBrojaTri;
    int l;
    for(i=0;i<n;i++)
        for(j=i+1;j<n+1;j++)
            for(k=j+1;k<n+2;k++)
                for(l=k+1;l<n+3;l++)
                    cetiriSabirkaBrojaTri.push_back(pow(3,i)+pow(3,j)+pow(3,k)+pow(3,l));

    vector<long long> petSabirkaBrojaTri;
    int m;
    for(i=0;i<n;i++)
        for(j=i+1;j<n+1;j++)
            for(k=j+1;k<n+2;k++)
                for(l=k+1;l<n+3;l++)
                    for(m=l+1;m<n+4;m++)
                        petSabirkaBrojaTri.push_back(pow(3,i)+pow(3,j)+pow(3,k)+pow(3,l)+pow(3,m));

    vector<long long> konacniSkup;
    for(i=0;i<stepeniBrojaTri.size();i++)
        konacniSkup.push_back(stepeniBrojaTri[i]);
    for(i=0;i<dvaSabirkaBrojaTri.size();i++)
        konacniSkup.push_back(dvaSabirkaBrojaTri[i]);
    for(i=0;i<triSabirkaBrojaTri.size();i++)
        konacniSkup.push_back(triSabirkaBrojaTri[i]);
    for(i=0;i<cetiriSabirkaBrojaTri.size();i++)
        konacniSkup.push_back(cetiriSabirkaBrojaTri[i]);
    for(i=0;i<petSabirkaBrojaTri.size();i++)
        konacniSkup.push_back(petSabirkaBrojaTri[i]);
    sort(konacniSkup.begin(), konacniSkup.end());

    int x;
    cin >> x;

    cout << konacniSkup[x-1] << endl;

    system("pause");
    return 0;
}
