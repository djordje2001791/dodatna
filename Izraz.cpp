#include <iostream>
#include <string>
#include <sstream>
#include <cstdlib>

using namespace std;

int main()
{
    string cijeliIzraz;
    string prviBrojTemp, drugiBrojTemp;
    long prviBroj, drugiBroj;
    string znak;

    cin >> cijeliIzraz;

    int i, n=cijeliIzraz.length();
    for(i=0;i<n;i++){
        if(cijeliIzraz.substr(i,1)=="+" ||
           cijeliIzraz.substr(i,1)=="-" ||
           cijeliIzraz.substr(i,1)=="*" ||
           cijeliIzraz.substr(i,1)=="/" ||
           cijeliIzraz.substr(i,1)=="%")
        {

            prviBrojTemp = cijeliIzraz.substr(0,i);
            drugiBrojTemp = cijeliIzraz.substr(i+1, n-1-i);
            znak = cijeliIzraz.substr(i,1);

            stringstream ss(prviBrojTemp);
            ss >> prviBroj;
            stringstream ss1(drugiBrojTemp);
            ss1 >> drugiBroj;

            break;
        }
    }

    if(znak=="+")
        cout << prviBroj + drugiBroj << endl;
    else if(znak=="-")
        cout << prviBroj - drugiBroj << endl;
    else if(znak=="*")
        cout << prviBroj * drugiBroj << endl;
    else if(znak=="/")
        cout << prviBroj / drugiBroj << endl;
    else
        cout << prviBroj % drugiBroj << endl;

    system("pause");
    return 0;
}
