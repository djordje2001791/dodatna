#include <iostream>
#include <math.h>
#include <cstdlib>
#include <sstream>

using namespace std;

long konvertujPromjenjuvu(string a){
	long b;

	istringstream ss(a);
	ss >> b;

    return b;
}

long konvertujSistem(long baza, string broj){
    long i,j;
    string a[20];
    if(baza==10)
        return konvertujPromjenjuvu(broj);

    for(i=0;i<broj.length();i++){
        a[i]=broj.substr(i,1);
        if(a[i]=="A")
            a[i]="10";
        else if(a[i]=="B")
            a[i]="11";
        else if(a[i]=="C")
            a[i]="12";
        else if(a[i]=="D")
            a[i]="13";
        else if(a[i]=="E")
            a[i]="14";
        else if(a[i]=="F")
            a[i]="15";
        else if(a[i]=="G")
            a[i]="16";
        else if(a[i]=="I")
            a[i]="17";
        else if(a[i]=="J")
            a[i]="18";
        else if(a[i]=="K")
            a[i]="19";
        else if(a[i]=="L")
            a[i]="20";
    }

    int n=i;

    int sum=0;
    for(i=0, j=n-1;i<n;i++,j--){
        sum+=konvertujPromjenjuvu(a[i])*pow(baza,j);
    }

    return sum;
}
int main()
{
    long b;
    string n;
    cin >> b;
    cin >> n;

    cout << konvertujSistem(b, n) << endl;

    system("pause");
    return 0;
}
