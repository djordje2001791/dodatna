#include <iostream>
#include <cstdlib>
#include <math.h>
#include <vector>

using namespace std;

bool jeProst(int a){
	int i, j, flag = 0;

	for (i = 2; i <= a / 2; ++i)
	{
		if (a%i == 0)
		{
			flag = 1;
			break;
		}
	}

	if(flag==0)
		return true;
	else
		return false;
}
int main()
{
    int n=0;
    cin >> n;

    vector<int> a;
    int i;
    for(i=2;i<n;i++){
        if(n%i==0){
            a.push_back(i);
        }
    }

    for(i=a.size()-1;i>=0;i--){
        if(jeProst(a[i])==true)
            cout << a[i] << " ";
    }

    cout << endl;
    system("pause");
    return 0;
}
