#include <iostream>

using namespace std;

typedef long long ll;

ll max(ll a, ll b){
    return (a>b) ? a : b;
}

int main(){
    ll x11, y11, x12, y12,
       x21, y21, x22, y22;

    cin >> x11 >> y11 >> x12 >> y12
        >> x21 >> y21 >> x22 >> y22;

    x11 = min(x11, x12), x12 = max(x11, x12);
    y11 = min(y11, y12), y12 = max(y11, y12);

    x21 = min(x21, x22), x22 = max(x21, x22);
    y21 = min(y21, y22), y22 = max(y21, y22);

    ll sideX = max(x12, x22) - max(x11, x21), sideY = max(y12, y22) - max(y11, y12);

    cout << sideX * sideY << endl;

    return 0;
}
