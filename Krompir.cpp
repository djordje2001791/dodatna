#include <iostream>
#include <vector>

using namespace std;

typedef unsigned long long ull;

int main(){
    ull n, m;
    cin >> n >> m;

    vector<ull> x, y;
    x.resize(m);
    y.resize(m);

    for(ull i=0; i<m; i++)
        cin >> x[i] >> y[i];

    vector<vector<bool> > field;
    field.resize(n);
    for(ull i=0; i<n; i++)
        field[i].resize(n,false);

    for(ull i=0; i<m; i++){
        for(ull j=0; j<n; j++)
            field[x[i]-1][j] = true;
        for(ull j=0; j<n; j++)
            field[j][y[i]-1] = true;
    }

    ull counter = 0;
    for(ull i=0; i<n; i++)
        for(ull j=0; j<n; j++)
            counter += (field[i][j]);

    cout << counter << endl;

    return 0;
}
