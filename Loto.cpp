#include <iostream>
#include <math.h>
#include <algorithm>
#include <cstdlib>

using namespace std;

inline long faktorijel(long x) {
	return (x == 1 ? x : x * faktorijel(x - 1));
}

long zbirCifara(long n) {
	long t=n, r, sum = 0;

	while (t>0)
	{
		r = t % 10;
		sum += r;
		t = t / 10;
	}

	return sum;
}

int main() {
	long N, M;
	cin >> N >> M;

	long C = faktorijel(N) / (faktorijel(N - M)*faktorijel(M));

	cout << zbirCifara(C) << endl;

	system("pause");
	return 0;
}

