#include "funkcije.h"

int main() {
	inicijalizacija_unos();

	for (ll i = 0; i < m; i++) {
		if (stanice[deonice[i].dstart].z > stanice[deonice[i].dkraj].z) {
			visa = deonice[i].dstart;
			niza = deonice[i].dkraj;
		} else {
			visa = deonice[i].dkraj;
			niza = deonice[i].dstart;
		}

		put = duzina(stanice[visa], stanice[niza]);
		brzina = k*sinus(stanice[visa], stanice[niza], put);
		vreme = put / brzina + deonice[i].brzastava*vremezast;
		graf[visa][niza] = vreme;
	}

	rastojanja[start] = 0;

	while ((u = dajNajboljeg()) != NEDEFINISANO) {
		obidjen[u] = true;
		for(ll v=0; v<n; v++)
			if (graf[u][v] != INF && !obidjen[v] && rastojanja[u] + graf[u][v] < rastojanja[v]) {
				rastojanja[v] = rastojanja[u] + graf[u][v];
				prethodnik[v] = u;
			}
	}

	cout << "Najkraci put je " << rastojanja[kraj] << endl;
	stampajPut(kraj);
	
	return 0;	
}