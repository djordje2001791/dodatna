#include <iostream>
#include <cstdlib>
#include <math.h>

using namespace std;

int main()
{
    unsigned long long min = 0, max = 0;
    unsigned long counter = 0;
    cin >> min >> max;

    unsigned int i = 0;
    for(i=min;i<=max;i++){
        if(sqrt(i)==floor(sqrt(i)))
            counter++;
    }

    cout << counter;

    system("pause");
    return 0;
}
