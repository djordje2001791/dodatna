#include <iostream>
#include <vector>
#include <string>
#include <cstdlib>

using namespace std;

int main()
{
    string temp;
    int temp1;
    vector<string> a;
    while(temp!="zadnja"){
        cin >> temp;
        a.push_back(temp);
    }

    a.erase(a.end()-1);

    int i,j,k;
    char b[a.size()][100];
    int asciiCodes[a.size()][100];
    int tezina[a.size()];
    string najteza;
    for(i=0;i<a.size();i++){
        for(j=0;j<a[i].length();j++){
            b[i][j]=a[i].at(i);
            asciiCodes[i][j]=(int)b[i][j];
        }
    }

    for(i=0;i<a.size();i++){
        for(j=0;j<a[i].length()-1;j++){
            for(k=j+1;k<a[i].length();k++){
                if(asciiCodes[i][j]+1==asciiCodes[i][k] || asciiCodes[i][j]-1==asciiCodes[i][k])
                    tezina[i]++;
            }
        }
    }

    for(i=0;i<a.size()-1;i++){
        for(j=i+1;j<a.size();j++){
            if(tezina[j]>tezina[i]){
                temp1=tezina[j];
                tezina[j]=tezina[i];
                tezina[i]=temp1;

                temp=a[j];
                a[j]=a[i];
                a[i]=temp;
            }
        }
    }

    cout << a[1] << endl;

    system("pause");
    return 0;
}
