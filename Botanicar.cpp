#include <iostream>
#include <vector>
#include <minmax.h>

#define UNDEFINED -1

using namespace std;

typedef unsigned long long ull;

vector<vector<bool> > field;
vector<vector<long long> > maxResult;
ull n, m;

bool valid(ull r, ull c) { return r < m && c < n; }

ull maxPathSum(ull row = 0, ull column = 0) {
	if (maxResult[row][column] == UNDEFINED) {
		if (!valid(row, column))
			return 0;

		if (row == n - 1 && column == m - 1) //base condition
			return field[row][column];

		ull path1 = maxPathSum(row + 1, column); //Right
		ull path2 = maxPathSum(row, column + 1); //Down

		maxResult[row][column] = field[row][column] + max(path1, path2);
		return maxResult[row][column];
	}
	else
		return maxResult[row][column];
}

int main() {
	cin >> n >> m;

	field.resize(m);
	for (ull i = 0; i < m; i++)
		field[i].resize(n);

	maxResult.resize(m);
	for (ull i = 0; i < m; i++)
		maxResult[i].resize(n, UNDEFINED);

	for (ull i = 0; i < m; i++)
		for (ull j = 0; j < n; j++) {
			char temp; 
			cin >> temp;
			field[i][j] = temp=='#';
		}
			
	cout << maxPathSum() << endl;

	return 0;
}