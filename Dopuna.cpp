#include <iostream>
#include <math.h>

using namespace std;

typedef double ull;

int main(){
    ull x;
    cin >> x;

    ull y=1, k=0.1;
    while(k != round(k)){
        k=sqrt(x*y);
        y++;
    }

    cout << --y << endl;

    return 0;
}
